import ProfileActions from "../components/Profile/ProfileActions"
import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileOrderHistory from "../components/Profile/ProfileOrderHistory"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"

const Profile = () => {

    const { user } = useUser();

    return (
        <>
            <h1>Profile</h1>
            <ProfileHeader username={user.username}></ProfileHeader>
            <ProfileActions></ProfileActions>
            <ProfileOrderHistory orders ={user.orders}></ProfileOrderHistory>
        </>
    )
}

export default withAuth(Profile)