import { useForm } from "react-hook-form"

const OrdersForm = () => {
    const { register, handleSubmit, } = useForm();

    const onSubmit = data => {
        console.log(data);
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <fieldset>
                <label htmlFor="order-notes">Order note:</label>
                <input type='text' {...register('orderNotes')} placeholder="No sugar, extra milk"></input>
            </fieldset>

            <button type="submit">Order</button>
        </form>
    )

}
export default OrdersForm