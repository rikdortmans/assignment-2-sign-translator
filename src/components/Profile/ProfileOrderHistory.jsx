import { nanoid } from "nanoid";
import ProfileOrderHistoryItem from "./ProfileOrderHistoryItem";

const ProfileOrderHistory = ({ orders }) => {

    const orderList = orders.map(order => <ProfileOrderHistoryItem key={nanoid()} order={order} />)
    return (
        <section>
            <h4>Your order history</h4>
            <ul>
                {orderList}
            </ul>
        </section>
    )
}

export default ProfileOrderHistory;