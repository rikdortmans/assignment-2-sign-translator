import axios from "axios"
 
const API_URL = process.env.REACT_APP_API_URL
const API_KEY = process.env.REACT_APP_API_KEY

const checkForUser = async (username) => {
    try {
        const response = await axios.get(`${API_URL}?username=${username}`);
        if(response.status !== 200){
            throw new Error('Could not complete request');
        }

        return [null, response.data]
        
    } catch (error) {
        return [ error.message, []]
    }
}

const createUser = async (username) => {

    const headers = {
        'Content-Type': 'application/json',
        'x-api-key': API_KEY,
    }

    const newUser = {
        username,
        orders:[],
    }

    try {
        const response = await axios.post(API_URL, newUser, {headers});

        if(response.status !== 201){
            throw new Error('Could not create user with username ' + username);
        }

        return [null, response.data]
        
    } catch (error) {
        return [ error.message, []]
    }

}

export const loginUser = async (username) =>{
    const [ checkError, user ] = await checkForUser(username);

    if(checkError !== null)
    {
        return [checkError, null];
    }

    if(user.length > 0){
        return  [null, user.pop()]
    }

    return await createUser(username);

}
